## sdm660_64-user 10 QKQ1 73 release-keys
- Manufacturer: asus
- Platform: sdm660
- Codename: ASUS_X01BD
- Brand: asus
- Flavor: sdm660_64-user
- Release Version: 10
- Id: QKQ1
- Incremental: 17.2018.2012.434-20201203
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: WW_Phone-17.2018.2012.434-20201203
- Branch: sdm660_64-user-10-QKQ1-73-release-keys
- Repo: asus_asus_x01bd_dump_13471


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
